#!/bin/bash

set -euxo pipefail

# PCSK9 in GRCh37/hg19
REGION="1:55505221-55530525"

SAMPLE_ID="$(cat input.json | jq -r '.sample_id')"
BAM_URL="$(cat input.json | jq -r '.files["alignment-bam"].url')"
BAI_URL="$(cat input.json | jq -r '.files["alignment-bai"].url')"
ANCESTRY_URL="$(cat input.json | jq -r '.files["ancestry-json"].url')"

echo $SAMPLE_ID

aws s3 cp s3://pipeline3.shared/reference_genome/human/g1k_v37/genome.fasta.gz .
aws s3 cp s3://pipeline3.shared/reference_genome/human/g1k_v37/genome.fasta.gz.fai .

curl --silent --show-error "$ANCESTRY_URL" | aws s3 cp - $OUTPUT_S3_PATH/$AWS_BATCH_JOB_ARRAY_INDEX/ancestry/$SAMPLE_ID.json
samtools mpileup -Q30 -q30 --ff DUP "$BAM_URL" -X "$BAI_URL" -f genome.fasta.gz --region $REGION | bgzip -c | aws s3 cp - $OUTPUT_S3_PATH/$AWS_BATCH_JOB_ARRAY_INDEX/mpileup/$SAMPLE_ID.pileup.gz
